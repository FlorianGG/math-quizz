﻿class Test
{
    enum functionOperator
    {
        MULTIPLY = 1,
        ADD = 2,
        SOUSTRACTION = 3
    }



    static void Main()
    {
        static int getNumber()
        {
            int number = 0;
            while (true)
            {
                Console.Write("Veuillez saisire la réponse: ");
                string nbString = Console.ReadLine();

                try
                {
                    number = int.Parse(nbString);
                    return number;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Vous devez entrer un nombre valide!");
                }
            }
        }

        static void app(int min, int max, int nbRound = 5)
        {
            Random rand = new Random();
            int points = 0;
            for (int i = 1; i <= nbRound; i++)
            {
                int numberA = rand.Next(min, max);
                int numberB = rand.Next(min, max);
                int result = 0;
                functionOperator op = (functionOperator)rand.Next(1, 4);

                switch (op)
                {
                    case functionOperator.MULTIPLY:
                        Console.WriteLine($"Quel est le résultat de: {numberA} * {numberB} ?");
                        result = numberA * numberB;
                        break;
                    case functionOperator.ADD:
                        Console.WriteLine($"Quel est le résultat de: {numberA} * {numberB} ?");
                        result = numberA * numberB;
                        break;
                    case functionOperator.SOUSTRACTION:
                        Console.WriteLine($"Quel est le résultat de: {numberA} - {numberB} ?");
                        result = numberA - numberB;
                        break;
                    default:
                        Console.WriteLine($"Opérateur incorrect!");
                        return;
                        
                }
                int tryResult = getNumber();

                if (tryResult == result)
                {
                    Console.WriteLine($"Bonne réponse!");
                    points++;
                }
                else
                {
                    Console.WriteLine($"Mauvaise réponse!");
                }
                Console.WriteLine();
            }

             int moyenne = points / nbRound;

            Console.WriteLine($"Vous avez répondu juste à {points} questions!");
            if (points == nbRound)
            {
                Console.WriteLine("Excellent!");
            }else if(points == 0)
            {
                Console.WriteLine("Revisez vos maths!");
            }else if(points <= moyenne)
            {
                Console.WriteLine("Insuffisant!");
            }
            else
            {
                Console.WriteLine("Pas mal!");
            }
            
        }

        const int min = 1;
        const int max = 10;
        const int nbRound = 5;
        app(min, max, nbRound);

    }

}















